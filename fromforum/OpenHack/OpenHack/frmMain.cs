﻿/*
Copyright 2012 DaCoder
This file is part of daCoders Tool for WoW 1.12.1.
daCoders Tool for WoW 1.12.1 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
daCoders Tool for WoW 1.12.1 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
*/
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

namespace OpenHack
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }
        private void cmbPID_MouseDown(object sender, MouseEventArgs e)
        {
            cmbPID.Items.Clear();


            foreach (Process process in Process.GetProcessesByName("Wow"))
            {
                cmbPID.Items.Add(process.Id);
            }
        }

        private void cmbPID_SelectedIndexChanged(object sender, EventArgs e)
        {
            int pid = int.Parse(cmbPID.Text);

            Globals.Process = Process.GetProcessById(pid);
            Globals.Magic.OpenProcessAndThread(pid);

            Offsets.Initialize();
        }

        private Boolean init()
        {
            GameReader WoW = Globals.WoWGame;
            WoW.init();
            Boolean retVal = WoW.LoadAddresses();           

            return retVal;
        }

        private void dataObjectList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex == -1)
                dataObjectList.Rows.Clear();
            else
                teleportTo(
                    (float)Convert.ToDouble(dataObjectList.Rows[e.RowIndex].Cells[3].Value),
                    (float)Convert.ToDouble(dataObjectList.Rows[e.RowIndex].Cells[4].Value),
                    (float)Convert.ToDouble(dataObjectList.Rows[e.RowIndex].Cells[5].Value)
                    );
        }

        private void dataObjectList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
            Clipboard.SetDataObject(dataObjectList.Rows[e.RowIndex].Cells[e.ColumnIndex].Value);
        }

        private void teleportTo(float X, float Y, float Z)
        {
            GameReader WoW = Globals.WoWGame;
            WoW.teleportTo(X, Y, Z);
        }


        private void UpdateInfo()
        {
            
            GameReader WoW = Globals.WoWGame;

            WowObject LocalPlayer = WoW.getLocalPlayer();
            WowObject LocalTarget = WoW.getLocalTarget();

            
            txtDebugOutput.Text = "";
            dataObjectList.Rows.Clear();
            


            ListWoWObjects list = WoW.getAllObjects();
            WowObject CurrentObject;

            // read the object manager from first object to last.
            for (int i = 0; i < list.Count; i++)
            {
                CurrentObject = list.Get(i);
                String typeName = "";               


                switch (CurrentObject.Type)
                {
                    case (short)Constants.ObjType.OT_UNIT: // an npc
                        typeName = "NPC";
                        break;
                    case (short)Constants.ObjType.OT_PLAYER: // a player
                        typeName = "Player";
                        break;
                    case (short)Constants.ObjType.OT_GAMEOBJ:                        
                        switch (CurrentObject.GameObjectType)
                        {
                            case (uint)Constants.GameObjectType.Chest:
                                // Herbs and Minerals
                                typeName = "Chest";
                                break;                      
                            default:
                                typeName = "GO "+ CurrentObject.GameObjectType.ToString();
                                break;
                        }
                        break;
                }
                if (typeName == "") typeName = CurrentObject.Type.ToString();

                dataObjectList.Rows.Add(typeName,
                    //CurrentObject.GameObjectType.ToString(),
                    CurrentObject.BaseAddress.ToString("X"),
                    CurrentObject.Name,
                    CurrentObject.XPos.ToString(),
                    CurrentObject.YPos.ToString(),
                    CurrentObject.ZPos.ToString()
                    );
            }


            //txtDebugOutput.Text += WoW.getClientConnectionAddress() + "\r\n";
            txtDebugOutput.Text += WoW.getObjectManagerAddress() + "\r\n";
            txtDebugOutput.Text += WoW.getFirstObjectAddress() + "\r\n";
            txtDebugOutput.Text += "Base Address: 0x" + Offsets.WoW.baseAddress.ToString("X") + "\r\n";

            txtDebugOutput.Text += "Player Name: " + LocalPlayer.Name + "\r\n";
            txtDebugOutput.Text += "Player Level: " + LocalPlayer.Level.ToString() + "\r\n";
            txtDebugOutput.Text += "Player Health: " + LocalPlayer.CurrentHealth.ToString() + " / " + LocalPlayer.MaxHealth.ToString() + "\r\n";
            txtDebugOutput.Text += "Player X-Pos: " + LocalPlayer.XPos.ToString() + "\r\n";
            txtDebugOutput.Text += "Player Y-Pos: " + LocalPlayer.YPos.ToString() + "\r\n";
            txtDebugOutput.Text += "Player Z-Pos: " + LocalPlayer.ZPos.ToString() + "\r\n";
            txtDebugOutput.Text += "Player Guid: " + string.Format("0x{0:X}", LocalPlayer.Guid) + "\r\n";
            txtDebugOutput.Text += "Player Rotation (Radians): " + Math.Round(LocalPlayer.Rotation, 2).ToString() + "\r\n";


            if (LocalTarget != null)
            {
                txtDebugOutput.Text += "Target Name: " + LocalTarget.Name + "\r\n";
                txtDebugOutput.Text += "Target Level: " + LocalTarget.Level.ToString() + "\r\n";
                txtDebugOutput.Text += "Target Health: " + LocalTarget.CurrentHealth.ToString() + " / " + LocalTarget.MaxHealth.ToString() + "\r\n";
                txtDebugOutput.Text += "Target X-Pos: " + LocalTarget.XPos.ToString() + "\r\n";
                txtDebugOutput.Text += "Target Y-Pos: " + LocalTarget.YPos.ToString() + "\r\n";
                txtDebugOutput.Text += "Target Z-Pos: " + LocalTarget.ZPos.ToString() + "\r\n";
                txtDebugOutput.Text += "Target Guid: " + string.Format("0x{0:X}", LocalTarget.Guid) + "\r\n";
                if (LocalTarget.SummonedBy > 0)
                    txtDebugOutput.Text += "Summoned By: " + WoW.PlayerNameFromGuid(LocalTarget.SummonedBy) + "\r\n";
            }


            txtDebugOutput.Text += "Total Objects In Manager: " + list.Count.ToString() + "\r\n";

        }

        private void btnInitHacks_Click(object sender, EventArgs e)
        {
            GameReader WoW = Globals.WoWGame;
            init();
            WoW.initHacks();
        }

        private void btnExample1_Click(object sender, EventArgs e)
        {
            GameReader WoW = Globals.WoWGame;
            ListWoWObjects list = WoW.getAllObjects();
            WowObject CurrentObject;
            // read the object manager from first object to last.
            for (int i = 0; i < list.Count; i++)
            {
                CurrentObject = list.Get(i);
                switch (CurrentObject.Type)
                {
                    case (short)Constants.ObjType.OT_GAMEOBJ:
                        if (CurrentObject.GameObjectType == (uint)Constants.GameObjectType.Chest)
                        {
                            switch (CurrentObject.Name)
                            {
                                case "Friedensblume":
                                case "Silberblatt":
                                    teleportTo(CurrentObject.XPos, CurrentObject.YPos, CurrentObject.ZPos);
                                    return;
                                default:
                                    break;
                            }
                        }
                        break;
                }
            }
        }

        private void btnExample2_Click(object sender, EventArgs e)
        {
            GameReader WoW = Globals.WoWGame;
            WoW.changePlayerSpeed(WoW.getPlayerSpeed() + (float)20.0);
        }

        private void btnUpdateObjects_Click(object sender, EventArgs e)
        {
            init();
            UpdateInfo();
        }

       

       
    }
}
