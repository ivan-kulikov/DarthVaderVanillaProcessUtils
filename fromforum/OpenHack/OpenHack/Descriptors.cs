﻿/*
Copyright 2012 DaCoder
This file is part of daCoders Tool for WoW 1.12.1.
daCoders Tool for WoW 1.12.1 is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
daCoders Tool for WoW 1.12.1 is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Foobar. If not, see http://www.gnu.org/licenses/.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenHack
{
    class Descriptors
    {
        /// <summary>
        ///    1.12.1.5875
        /// </summary>
        internal enum WoWUnitFields 
        {
            Charm = 0x18 / 4,
            Summon = 0x20 / 4,
            CharmedBy = 0x28 / 4,
            SummonedBy = 0x30 / 4,
            CreatedBy = 0x38 / 4,
            Target = 0x40 / 4,
            ChannelObject = 0x50 / 4,
            Health = 0x58 / 4,
            Power = 0x5c / 4,
            MaxHealth = 0x70 / 4,
            MaxPower = 0x74 / 4,
            Level = 0x88 / 4,
        }
    }
}
